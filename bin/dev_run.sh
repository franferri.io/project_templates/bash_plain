#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${ORIGINAL_SCRIPT_PATH}/libs/debug"
source "${ORIGINAL_SCRIPT_PATH}/libs/functions"

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd .. || exit 1

print_random_banner

print_execution_environment_information

echo

echo "Hola mundo!"

exit 0