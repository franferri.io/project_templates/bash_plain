# Example empty project

> https://franferri.io/templates

* Clone to get with: `git clone --depth 1 --recurse-submodules --shallow-submodules <repository-url>`
* Compile and run with: `./bin/dev_run.sh`
